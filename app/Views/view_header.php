<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">

    <!-- viewport meta -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?= $description ?>">
    <meta name="keywords" content="uin, suka">


    <title><?= $title ?></title>

    <!-- inject:css -->
    <link rel="stylesheet" href="<?= site_url('assets/') ?>css/animate.css">
    <link rel="stylesheet" href="<?= site_url('assets/') ?>css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= site_url('assets/') ?>css/fontello.css">
    <link rel="stylesheet" href="<?= site_url('assets/') ?>css/jquery-ui.css">
    <link rel="stylesheet" href="<?= site_url('assets/') ?>css/lnr-icon.css">
    <link rel="stylesheet" href="<?= site_url('assets/') ?>css/owl.carousel.css">
    <link rel="stylesheet" href="<?= site_url('assets/') ?>css/slick.css">
    <link rel="stylesheet" href="<?= site_url('assets/') ?>css/trumbowyg.min.css">
    <link rel="stylesheet" href="<?= site_url('assets/') ?>css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="<?= site_url('assets/') ?>style.css">
    <!-- endinject -->

    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?= site_url('assets/') ?><?= site_url('assets/') ?>images/favicon.png">
</head>

<body class="preload home1 mutlti-vendor">

    <!-- ================================
    START MENU AREA
================================= -->
    <!-- start menu-area -->
    <div class="menu-area">
        <!-- start .top-menu-area -->
        <div class="top-menu-area">
            <!-- start .container -->
            <div class="container">
                <!-- start .row -->
                <div class="row">
                    <!-- start .col-md-3 -->
                    <div class="col-lg-3 col-md-3 col-6 v_middle">
                        <div class="logo">
                            <a href="<?= site_url() ?>">
                                <img src="<?= site_url('assets/') ?>images/logo.png" alt="logo image" class="img-fluid" style="height: 70px;">
                            </a>
                        </div>
                    </div>
                    <!-- end /.col-md-3 -->

                    <!-- start .col-md-5 -->
                    <div class="col-lg-8 offset-lg-1 col-md-9 col-6 v_middle">
                        <!-- start .author-area -->
                        <div class="author-area">
                            
                            <div class="author__notification_area">
                                <ul>

                                    <li class="has_dropdown">
                                        <div class="icon_wrap">
                                            <span class="lnr lnr-cart"></span>
                                            <span class="notification_count purch">0</span>
                                        </div>

                                        <div class="dropdowns dropdown--cart">
                                            <div class="cart_area">

                                                <center><h5 style="margin-top: 20px; margin-bottom: 20px;">Keranjang Anda Masih Kosong</h5></center>
<!--                                                 
                                                <div class="cart_product">
                                                    <div class="product__info">
                                                        <div class="thumbn">
                                                            <img src="<?= site_url('assets/') ?>images/capro1.jpg" alt="cart">
                                                        </div>

                                                        <div class="info">
                                                            <a class="title" href="single-product.html">Finance and Consulting Business Theme</a>
                                                            <div class="cat">
                                                                <a href="#">
                                                                    <img src="<?= site_url('assets/') ?>images/catword.png" alt="">Wordpress</a>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="product__action">
                                                        <a href="#">
                                                            <span class="lnr lnr-trash"></span>
                                                        </a>
                                                        <p>$60</p>
                                                    </div>
                                                </div> -->
                                                
                                                <!-- <div class="total">
                                                    <p><span>Total :</span>$80</p>
                                                </div> -->

                                                <div class="cart_action">
                                                    <a class="go_cart" href="cart.html">Lihat Keranjang</a>
                                                    <a class="go_checkout" href="checkout.html">Checkout</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <!--start .author__notification_area -->

                            <!--start .author-author__info-->
                            <div class="author-author__info inline has_dropdown">
                                <div class="author__avatar">
                                    <img src="<?= site_url('assets/') ?>images/usr_avatar.png" alt="">

                                </div>
                                <div class="autor__info">
                                    <p class="name">
                                        Jhon Doe
                                    </p>
                                </div>

                                <div class="dropdowns dropdown--author">
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <span class="lnr lnr-cog"></span>Pengaturan Akun</a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="lnr lnr-question-circle"></span>Layanan Konsumen</a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="lnr lnr-cross"></span>Pembatalan Pesanan</a>
                                        </li>
                                        <li>
                                            <a href="#"><span class="lnr lnr-exit"></span>Logout</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!--end /.author-author__info-->
                        </div>
                        <!-- end .author-area -->

                        <!-- author area restructured for mobile -->
                        <div class="mobile_content ">
                            <span class="lnr lnr-user menu_icon"></span>

                            <!-- offcanvas menu -->
                            <div class="offcanvas-menu closed">
                                <span class="lnr lnr-cross close_menu"></span>
                                <div class="author-author__info">
                                    <div class="author__avatar v_middle">
                                        <img src="<?= site_url('assets/') ?>images/usr_avatar.png" alt="">
                                    </div>
                                    <div class="autor__info v_middle">
                                        <p class="name">
                                            Jhon Doe
                                        </p>
                                    </div>
                                </div>
                                <!--end /.author-author__info-->

                                <div class="dropdowns dropdown--author">
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <span class="lnr lnr-cog"></span>Pengaturan Akun</a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="lnr lnr-question-circle"></span>Layanan Konsumen</a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="lnr lnr-cross"></span>Pembatalan Pesanan</a>
                                        </li>
                                        <li>
                                            <a href="#"><span class="lnr lnr-exit"></span>Logout</a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="text-center social social--color--filled">
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <span class="fa fa-facebook"></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="fa fa-twitter"></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="fa fa-youtube"></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="fa fa-instagram"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- end /.mobile_content -->
                    </div>
                    <!-- end /.col-md-5 -->
                </div>
                <!-- end /.row -->
            </div>
            <!-- end /.container -->
        </div>
        <!-- end  -->

        <!-- start .mainmenu_area -->
        <div class="mainmenu">
            <!-- start .container -->
            <div class="container">
                <!-- start .row-->
                <div class="row">
                    <!-- start .col-md-12 -->
                    <div class="col-md-12">
                        <div class="navbar-header">
                            <!-- start mainmenu__search -->
                            <div class="mainmenu__search social social--color--filled">
                                <ul>
                                    <li>
                                        <a href="#">
                                            <span class="fa fa-facebook"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span class="fa fa-twitter"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span class="fa fa-youtube"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span class="fa fa-instagram"></span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            
                        </div>

                        <nav class="navbar navbar-expand-md navbar-light mainmenu__menu">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
                                aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="navbarNav">
                                <ul class="navbar-nav">
                                    <li>
                                        <a href="<?= site_url('home/') ?>">Home</a>
                                    </li>
                                    <li class="has_dropdown">
                                        <a href="<?= site_url('layanan/') ?>">Layanan</a>
                                        <div class="dropdowns dropdown--menu">
                                            <ul>
                                                <li>
                                                    <a href="<?= site_url('toko/') ?>">Toko</a>
                                                </li>
                                                <li>
                                                    <a href="<?= site_url('food-court/') ?>">Food Court</a>
                                                </li>
                                                <li>
                                                    <a href="<?= site_url('futsal/') ?>">Futsal</a>
                                                </li>
                                                <li>
                                                    <a href="<?= site_url('tenis/') ?>">Tenis</a>
                                                </li>
                                                <li>
                                                    <a href="<?= site_url('gedung/') ?>">Gedung</a>
                                                </li>
                                                <li>
                                                    <a href="<?= site_url('club-house/') ?>">Club House</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li>
                                        <a href="<?= site_url('kontak/') ?>">Kontak Kami</a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.navbar-collapse -->
                        </nav>
                    </div>
                    <!-- end /.col-md-12 -->
                </div>
                <!-- end /.row-->
            </div>
            <!-- start .container -->
        </div>
        <!-- end /.mainmenu-->
    </div>
    <!-- end /.menu-area -->
    <!--================================
    END MENU AREA
=================================-->

    <style>
        html {
        scroll-behavior: smooth;
        }
    </style>
