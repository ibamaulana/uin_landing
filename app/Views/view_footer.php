

    <!--================================
    START CALL TO ACTION AREA
=================================-->
<section class="call-to-action bgimage">
        <div class="bg_image_holder">
            <img src="<?= site_url('assets/') ?>images/cta.jpg" alt="">
        </div>
        <div class="container content_above">
            <div class="row">
                <div class="col-md-6 offset-md-6">
                    <div class="call-to-wrap">
                        <h1 class="text--white">Dapatkan Diskon Menarik!</h1>
                        <h4 class="text--white">Bagi Anda Pengguna Rutin Layanan UIN silahkan download Aplikasinya, Tersedia di IOS dan Android.</h4>
                        <a href="#" class="btn btn--lg btn--round btn--white callto-action-btn">Download Sekarang</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================================
    END CALL TO ACTION AREA
=================================-->


    <!--================================
    START FOOTER AREA
=================================-->
<footer class="footer-area">
        <div class="footer-big section--padding">
            <!-- start .container -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="info-footer">
                            <div class="info__logo">
                                <center><img style="width: 50%; margin-top: 10px;" src="<?= site_url('assets/') ?>images/logo_white.png" alt=""></center>
                            </div>
                        </div>
                        <!-- end /.info-footer -->
                    </div>
                    <!-- end /.col-md-3 -->

                    <div class="col-lg-3 col-md-6">
                        <div class="info-footer">
                            <h4 class="footer-widget-title text--white">Kontak Kami</h4>
                            <ul class="info-contact">
                                <li>
                                    <span class="lnr lnr-phone info-icon"></span>
                                    <span class="info">+62-274-512474<br>+62-274-589621</span>
                                </li>
                                <li>
                                    <span class="lnr lnr-envelope info-icon"></span>
                                    <span class="info">bisnis@uin-suka.ac.id</span>
                                </li>
                                <li>
                                    <span class="lnr lnr-map-marker info-icon"></span>
                                    <span class="info">Jl. Marsda Adisucipto Yogyakarta</span>
                                </li>
                            </ul>
                            <a href="#wa" class="btn btn--round btn--sm" style="margin-top: 10px;">Kontak Via Whatsapp</a>
                        </div>
                        <!-- end /.footer-menu -->
                    </div>
                    <!-- end /.col-md-5 -->
                    

                    <div class="col-lg-3 col-md-6">
                        
                        <div class="footer-menu" style="width: 100%;">
                            <h4 class="footer-widget-title text--white">Layanan</h4>
                            <ul>
                                <li>
                                    <a href="#">Toko</a>
                                </li>
                                <li>
                                    <a href="#">Food Court</a>
                                </li>
                                <li>
                                    <a href="#">Futsal</a>
                                </li>
                                <li>
                                    <a href="#">Tenis</a>
                                </li>
                                <li>
                                    <a href="#">Gedung</a>
                                </li>
                                <li>
                                    <a href="#">Club House</a>
                                </li>
                            </ul>
                        </div>

                    </div>


                    <div class="col-lg-3 col-md-12">
                        <div class="info-footer">
                            <h4 class="footer-widget-title text--white">Lokasi</h4>    
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15812.190187569378!2d110.3943589!3d-7.7847839!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x7f35156a4aeb645a!2sUIN%20Sunan%20Kalijaga%20Yogyakarta!5e0!3m2!1sid!2sid!4v1640191810624!5m2!1sid!2sid" width="100%" height="auto" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                        </div>
                        <!-- end /.footer-menu -->
                    </div>
                    <!-- end /.col-md-4 -->
                    

                </div>
                <!-- end /.row -->
            </div>
            <!-- end /.container -->
        </div>
        <!-- end /.footer-big -->

        <div class="mini-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="copyright-text">
                            <p>&copy; <?= date('Y') ?>
                                <a href="#">UIN SUKA</a>. All rights reserved. Created by
                                <a href="#">Developer</a>
                            </p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--================================
    END FOOTER AREA
=================================-->

    <!--//////////////////// JS GOES HERE ////////////////-->

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA0C5etf1GVmL_ldVAichWwFFVcDfa1y_c"></script>
    <!-- inject:js -->
    <script src="<?= site_url('assets/') ?>js/vendor/jquery/jquery-1.12.3.js"></script>
    <script src="<?= site_url('assets/') ?>js/vendor/jquery/popper.min.js"></script>
    <script src="<?= site_url('assets/') ?>js/vendor/jquery/uikit.min.js"></script>
    <script src="<?= site_url('assets/') ?>js/vendor/bootstrap.min.js"></script>
    <script src="<?= site_url('assets/') ?>js/vendor/chart.bundle.min.js"></script>
    <script src="<?= site_url('assets/') ?>js/vendor/grid.min.js"></script>
    <script src="<?= site_url('assets/') ?>js/vendor/jquery-ui.min.js"></script>
    <script src="<?= site_url('assets/') ?>js/vendor/jquery.barrating.min.js"></script>
    <script src="<?= site_url('assets/') ?>js/vendor/jquery.countdown.min.js"></script>
    <script src="<?= site_url('assets/') ?>js/vendor/jquery.counterup.min.js"></script>
    <script src="<?= site_url('assets/') ?>js/vendor/jquery.easing1.3.js"></script>
    <script src="<?= site_url('assets/') ?>js/vendor/owl.carousel.min.js"></script>
    <script src="<?= site_url('assets/') ?>js/vendor/slick.min.js"></script>
    <script src="<?= site_url('assets/') ?>js/vendor/tether.min.js"></script>
    <script src="<?= site_url('assets/') ?>js/vendor/trumbowyg.min.js"></script>
    <script src="<?= site_url('assets/') ?>js/vendor/waypoints.min.js"></script>
    <script src="<?= site_url('assets/') ?>js/dashboard.js"></script>
    <script src="<?= site_url('assets/') ?>js/main.js"></script>
    <script src="<?= site_url('assets/') ?>js/map.js"></script>
    <!-- endinject -->
</body>

</html>