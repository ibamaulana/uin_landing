
    
    <!--================================
        START BREADCRUMB AREA
    =================================-->
    <section class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-title"><b>Lapangan Futsal</b></h1>
                    <h2 style="color: white;">UIN Sunan Kalijaga</h2>
                </div>
                <!-- end /.col-md-12 -->
            </div>
            <!-- end /.row -->
        </div>
        <!-- end /.container -->
    </section>
    <!--================================
        END BREADCRUMB AREA
    =================================-->


    
    <!--============================================
        START SINGLE PRODUCT DESCRIPTION AREA
    ==============================================-->
    <section class="single-product-desc">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="item-preview">
                        <div class="item__preview-slider">
                            <div class="prev-slide">
                                <img src="<?= site_url('assets/') ?>images/futsal1.jpg" alt="">
                            </div>
                            <div class="prev-slide">
                                <img src="<?= site_url('assets/') ?>images/futsal2.jpg" alt="">
                            </div>
                            <div class="prev-slide">
                                <img src="<?= site_url('assets/') ?>images/futsal3.jpg" alt="">
                            </div>
                            <div class="prev-slide">
                                <img src="<?= site_url('assets/') ?>images/futsal4.jpg" alt="">
                            </div>
                            <div class="prev-slide">
                                <img src="<?= site_url('assets/') ?>images/futsal5.jpg" alt="">
                            </div>
                            <div class="prev-slide">
                                <img src="<?= site_url('assets/') ?>images/futsal6.jpg" alt="">
                            </div>
                        </div>
                        <!-- end /.item--preview-slider -->

                        <div class="item__preview-thumb">
                            <div class="prev-thumb">
                                <div class="thumb-slider">
                                    <div class="item-thumb">
                                        <img src="<?= site_url('assets/') ?>images/futsal1.jpg" alt="">
                                    </div>
                                    <div class="item-thumb">
                                        <img src="<?= site_url('assets/') ?>images/futsal2.jpg" alt="">
                                    </div>
                                    <div class="item-thumb">
                                        <img src="<?= site_url('assets/') ?>images/futsal3.jpg" alt="">
                                    </div>
                                    <div class="item-thumb">
                                        <img src="<?= site_url('assets/') ?>images/futsal4.jpg" alt="">
                                    </div>
                                    <div class="item-thumb">
                                        <img src="<?= site_url('assets/') ?>images/futsal5.jpg" alt="">
                                    </div>
                                    <div class="item-thumb">
                                        <img src="<?= site_url('assets/') ?>images/futsal6.jpg" alt="">
                                    </div>
                                </div>
                                <!-- end /.thumb-slider -->

                                <div class="prev-nav thumb-nav">
                                    <span class="lnr nav-left lnr-arrow-left"></span>
                                    <span class="lnr nav-right lnr-arrow-right"></span>
                                </div>
                                <!-- end /.prev-nav -->
                            </div>

                        </div>
                        <!-- end /.item__preview-thumb-->


                    </div>
                    <!-- end /.item-preview-->

                    <div class="item-info">
                        <div class="item-navigation">
                            <ul class="nav nav-tabs">
                                <li>
                                    <a href="#product-details" class="active" aria-controls="product-details" role="tab" data-toggle="tab"><center>Reguler</center></a>
                                </li>
                                <li>
                                    <a href="#product-comment" aria-controls="product-comment" role="tab" data-toggle="tab"><center>Turnamen</center></a>
                                </li>
                            </ul>
                        </div>
                        <!-- end /.item-navigation -->

                        <div class="tab-content">
                            <div class="fade show tab-pane product-tab active" id="product-details">
                                <div class="tab-content-wrapper">
                                    <h1>Syarat dan Ketentuan</h1>
                                    <p>Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the
                                        mattis, leo quam aliquet congue placerat mi id nisi interdum mollis. Praesent pharetra,
                                        justo ut scel erisque the mattis, leo quam aliquet congue justo ut scelerisque. Praesent
                                        pharetra, justo ut scelerisque the mattis, leo quam aliquet congue justo ut scelerisque.</p>

                                    <h1>Ketentuan Unggahan Dokumen</h1>
                                    <p>Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the
                                        mattis, leo quam aliquet congue placerat mi id nisi interdum mollis. Praesent pharetra,
                                        justo ut scel erisque the mattis, leo quam aliquet congue justo ut scelerisque. Praesent
                                        pharetra, justo ut scelerisque the mattis, leo quam aliquet congue justo ut scelerisque.</p>

                                    <h1>Cek Jadwal</h1>

                                </div>
                            </div>
                            <!-- end /.tab-content -->

                            <div class="fade tab-pane product-tab" id="product-comment">
                                <div class="tab-content-wrapper">

                                </div>
                            </div>
                            <!-- end /.product-comment -->

                        </div>
                        <!-- end /.tab-content -->
                    </div>
                    <!-- end /.item-info -->
                </div>
                <!-- end /.col-md-8 -->

                <div class="col-lg-4">
                    <aside class="sidebar sidebar--single-product">
                        <div class="sidebar-card card-pricing">
                            <ul class="pricing-options">
                                <li>
                                    <label for="opt1">
                                        <span class="pricing__opt">Alamat</span><br>
                                        <span class="circle"></span>Jl. Timoho No 64C, Papringan, Caturtunggal, Kec Depok, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55281
                                    </label>
                                </li>
                                <li>
                                    <label for="opt1">
                                        <span class="pricing__opt">Jam Operasional</span><br>
                                        <span class="circle"></span>08:00 - 21.00
                                    </label>
                                </li>
                            </ul>
                            <!-- end /.pricing-options -->
                        </div>
                        <!-- end /.sidebar--card -->

                        <div class="sidebar-card card--metadata">
                            <ul class="data">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3953.0341216838488!2d110.39418261477807!3d-7.786207094388801!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a59dc82b43df7%3A0x1859378c1627a11f!2sJl.%20Timoho%20No.64C%2C%20Papringan%2C%20Caturtunggal%2C%20Kec.%20Depok%2C%20Kabupaten%20Sleman%2C%20Daerah%20Istimewa%20Yogyakarta%2055281!5e0!3m2!1sid!2sid!4v1640220012076!5m2!1sid!2sid" width="100%" height="300" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                            </ul>
                        </div>
                        <!-- end /.sidebar-card -->

                    </aside>
                    <!-- end /.aside -->
                </div>
                <!-- end /.col-md-4 -->
            </div>
            <!-- end /.row -->
        </div>
        <!-- end /.container -->
    </section>
    <!--===========================================
        END SINGLE PRODUCT DESCRIPTION AREA
    ===============================================-->
