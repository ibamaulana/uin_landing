

    <!--================================
        START PRODUCTS AREA
    =================================-->
    <section class="products section--padding2">
        <!-- start container -->
        <div class="container">
            <!-- start .row -->
            <div class="row">

                <!-- start col-md-9 -->
                <div class="col-lg-9">
                    <div class="row">
                        
                    <div class="col-lg-4 col-md-6">
                            <!-- start .single-product -->
                            <div class="product product--card product--card-small">

                                <div class="product__thumbnail">
                                    <img src="<?= site_url('assets/') ?>images/hand_sanitizer.jpg" alt="">
                                </div>
                                <!-- end /.product__thumbnail -->

                                <div class="product-desc">
                                    <a class="product_title">
                                        <h4>Hand Sanitizer</h4>
                                    </a>
                                    <ul class="titlebtm">
                                        <li>
                                            <p>
                                                Rp. 20.000
                                            </p>
                                        </li>
                                    </ul>

                                </div>
                                <!-- end /.product-desc -->

                                <div class="product-purchase">
                                    <div class="price_love">
                                        <span style="background-color: #ba2c22; color: white;">-</span>
                                        <span>1</span>
                                        <span style="background-color: #179623; color: white;">+</span>
                                    </div>
                                </div>
                                <!-- end /.product-purchase -->
                            </div>
                            <!-- end /.single-product -->
                        </div>
                        <!-- end /.col-md-4 -->
                        
                        <div class="col-lg-4 col-md-6">
                            <!-- start .single-product -->
                            <div class="product product--card product--card-small">

                                <div class="product__thumbnail">
                                    <img src="<?= site_url('assets/') ?>images/masker.jpg" alt="">
                                </div>
                                <!-- end /.product__thumbnail -->

                                <div class="product-desc">
                                    <a class="product_title">
                                        <h4>Masker</h4>
                                    </a>
                                    <ul class="titlebtm">
                                        <li>
                                            <p>
                                                Rp. 50.000
                                            </p>
                                        </li>
                                    </ul>

                                </div>
                                <!-- end /.product-desc -->

                                <div class="product-purchase">
                                    <div class="price_love">
                                        <span style="background-color: #ba2c22; color: white;">-</span>
                                        <span>2</span>
                                        <span style="background-color: #179623; color: white;">+</span>
                                    </div>
                                </div>
                                <!-- end /.product-purchase -->
                            </div>
                            <!-- end /.single-product -->
                        </div>
                        <!-- end /.col-md-4 -->

                        <div class="col-lg-4 col-md-6">
                            <!-- start .single-product -->
                            <div class="product product--card product--card-small">

                                <div class="product__thumbnail">
                                    <img src="<?= site_url('assets/') ?>images/flashdisk.jpg" alt="">
                                </div>
                                <!-- end /.product__thumbnail -->

                                <div class="product-desc">
                                    <a class="product_title">
                                        <h4>Flashdisk</h4>
                                    </a>
                                    <ul class="titlebtm">
                                        <li>
                                            <p>
                                                Rp. 50.000
                                            </p>
                                        </li>
                                    </ul>

                                </div>
                                <!-- end /.product-desc -->

                                <div class="product-purchase">
                                    <div class="price_love">
                                        <span style="background-color: #179623; color: white;">+</span>
                                    </div>
                                </div>
                                <!-- end /.product-purchase -->
                            </div>
                            <!-- end /.single-product -->
                        </div>
                        <!-- end /.col-md-4 -->

                        <div class="col-lg-4 col-md-6">
                            <!-- start .single-product -->
                            <div class="product product--card product--card-small">

                                <div class="product__thumbnail">
                                    <img src="<?= site_url('assets/') ?>images/pena.jpg" alt="">
                                </div>
                                <!-- end /.product__thumbnail -->

                                <div class="product-desc">
                                    <a class="product_title">
                                        <h4>Pena</h4>
                                    </a>
                                    <ul class="titlebtm">
                                        <li>
                                            <p>
                                                Rp. 5.000
                                            </p>
                                        </li>
                                    </ul>

                                </div>
                                <!-- end /.product-desc -->

                                <div class="product-purchase">
                                    <div class="price_love">
                                        <span style="background-color: #179623; color: white;">+</span>
                                    </div>
                                </div>
                                <!-- end /.product-purchase -->
                            </div>
                            <!-- end /.single-product -->
                        </div>
                        <!-- end /.col-md-4 -->

                    </div>
                </div>
                <!-- end /.col-md-9 -->

                
                <!-- start .col-md-3 -->
                <div class="col-lg-3">
                    <!-- start aside -->
                    <aside class="sidebar product--sidebar">
                        <div class="sidebar-card card--category">
                            <a class="card-title" href="#collapse1" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapse1">
                                <h4>Rincian Pesanan
                                    <span class="lnr lnr-chevron-down"></span>
                                </h4>
                            </a>
                            <div class="collapse show collapsible-content" id="collapse1">
                                <ul class="card-content">

                                    <li>
                                        Hand Sanitizer <br>
                                        <span class="item-count">x1</span>
                                        <span style="float: right;"><b>Rp. 20.000</b></span>
                                    </li>
                                    <br>
                                    
                                    <li>
                                        Masker <br>
                                        <span class="item-count">x2</span>
                                        <span style="float: right;"><b>Rp. 10.000</b></span>
                                    </li>
                                    <br>
                                    
                                    <hr>
                                    
                                    <li>
                                        <span class="item-count"><b>Total</b></span>
                                        <span style="float: right;"><b>Rp. 30.000</b></span>
                                    </li>
                                    <br>

                                </ul>
                            </div>
                            <!-- end /.collapsible_content -->
                        </div>
                        <!-- end /.sidebar-card -->

                        
                        <div class="sidebar-card card--filter">
                            <a href="#wa" class="btn btn--lg" style="width: 100%;">Checkout</a>
                        </div>
                        <!-- end /.sidebar-card -->

                    </aside>
                    <!-- end aside -->
                </div>
                <!-- end /.col-md-3 -->


            </div>
            <!-- end /.row -->

        </div>
        <!-- end /.container -->

    </section>
    <!--================================
        END PRODUCTS AREA
    =================================-->
