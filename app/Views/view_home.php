
    <!--================================
        START HERO AREA
    =================================-->
    <section class="hero-area hero--2 bgimage">
        <div class="bg_image_holder">
            <img src="<?= site_url('assets/') ?>images/header.jpg" alt="">
        </div>

        <!-- start hero-content -->
        <div class="hero-content">
            <!-- start .contact_wrapper -->
            <div class="content-wrapper">
                <!-- start .container -->
                <div class="container">
                    <!-- start row -->
                    <div class="row">
                        <!-- start col-md-12 -->
                        <div class="col-md-6 offset-md-6">
                            <div class="hero__content__title" style="margin-bottom: 20px;">
                                <h3 style="margin-bottom: 20px;">
                                    Selamat Datang Di Layanan Terpadu
                                </h3>
                                <h2 style="font-size: 32px;">
                                    <b>Pusat Pengembangan Bisnis<br>UIN Sunan Kalijaga Yogyakarta</b>
                                </h2>
                            </div>

                            <!-- start .hero__btn-area-->
                            <div class="hero__btn-area">
                                <a href="#layanan" class="btn btn--round btn--lg">Pilih Layanan</a>
                            </div>
                            <!-- end .hero__btn-area-->
                        </div>
                        <!-- end /.col-md-12 -->
                    </div>
                    <!-- end /.row -->
                </div>
                <!-- end /.container -->
            </div>
            <!-- end .contact_wrapper -->
        </div>
        <!-- end hero-content -->

    <!--================================
        END HERO AREA
    =================================-->

    <!--================================
    START FEATURE AREA
=================================-->
    <section class="features section--padding" id="layanan">
        <!-- start container -->
        <div class="container">
            
            <!-- start row -->
            <div class="row">
                <!-- start col-md-12 -->
                <div class="col-md-12">
                    <div class="section-title">
                        <h1><b>Layanan</b>
                            <span class="highlighted">UIN SUKA</span>
                        </h1>
                        <p>Silahkan Pillih Layanan Menarik Dari UIN SUKA</p>
                    </div>
                </div>
                <!-- end /.col-md-12 -->
            </div>
            <!-- end /.row -->
            
            <!-- start row -->
            <div class="row">
                <!-- start search-area -->
                

                <div class="row col-12">

                
                        
                    <div class="col-lg-4 col-md-12" style="margin-top: 10px;">
                        <a href="<?= site_url('toko') ?>">
                            <div class="feature" style="box-shadow: 0 2px 50px rgb(0 0 0 / 8%); transition: 0.3s ease; border-radius: 30px;">
                                <div class="feature__img">
                                    <img src="<?= site_url('assets/') ?>images/layanan_toko.png" alt="feature" width="120">
                                </div>
                                <div class="feature__title">
                                    <h3><b>Toko</b></h3>
                                </div>
                                <!-- <div class="feature__desc">
                                    <a href="<?= site_url('layanan/toko/') ?>" class="btn btn--round btn--sm">Lihat</a>
                                </div> -->
                            </div>
                            <!-- end /.feature -->
                        </a>
                    </div>
                    <!-- end /.col-lg-4 col-md-6 -->

                    <!-- start search-area -->
                    <div class="col-lg-4 col-md-12" style="margin-top: 10px;">
                        <div class="feature" style="box-shadow: 0 2px 50px rgb(0 0 0 / 8%); transition: 0.3s ease; border-radius: 30px;">
                            <div class="feature__img">
                                <img src="<?= site_url('assets/') ?>images/layanan_food.png" alt="feature" width="120">
                            </div>
                            <div class="feature__title">
                                <h3><b>Food Court</b></h3>
                            </div>
                            <!-- <div class="feature__desc">
                                <a href="<?= site_url('layanan/food-court/') ?>" class="btn btn--round btn--sm">Lihat</a>
                            </div> -->
                        </div>
                        <!-- end /.feature -->
                    </div>
                    <!-- end /.col-lg-4 col-md-6 -->

                    <!-- start search-area -->
                    <div class="col-lg-4 col-md-12" style="margin-top: 10px;">
                        <a href="<?= site_url('futsal') ?>">
                            <div class="feature" style="box-shadow: 0 2px 50px rgb(0 0 0 / 8%); transition: 0.3s ease; border-radius: 30px;">
                                <div class="feature__img">
                                    <img src="<?= site_url('assets/') ?>images/layanan_futsal.png" alt="feature" width="120">
                                </div>
                                <div class="feature__title">
                                    <h3><b>Futsal</b></h3>
                                </div>
                            </div>
                            <!-- end /.feature -->
                        </a>
                    </div>
                <!-- end /.col-lg-4 col-md-6 -->

                </div>

                <div class="row col-12" style="margin-top: 20px;">

                    <!-- start search-area -->
                    <div class="col-lg-4 col-md-12" style="margin-top: 10px;">
                        <div class="feature" style="box-shadow: 0 2px 50px rgb(0 0 0 / 8%); transition: 0.3s ease; border-radius: 30px;">
                            <div class="feature__img">
                                <img src="<?= site_url('assets/') ?>images/layanan_tenis.png" alt="feature" width="120">
                            </div>
                            <div class="feature__title">
                                <h3><b>Tenis</b></h3>
                            </div>
                        </div>
                        <!-- end /.feature -->
                    </div>
                    <!-- end /.col-lg-4 col-md-6 -->

                    <!-- start search-area -->
                    <div class="col-lg-4 col-md-12" style="margin-top: 10px;">
                        <div class="feature" style="box-shadow: 0 2px 50px rgb(0 0 0 / 8%); transition: 0.3s ease; border-radius: 30px;">
                            <div class="feature__img">
                                <img src="<?= site_url('assets/') ?>images/layanan_gedung.png" alt="feature" width="120">
                            </div>
                            <div class="feature__title">
                                <h3><b>Gedung</b></h3>
                            </div>
                        </div>
                        <!-- end /.feature -->
                    </div>
                    <!-- end /.col-lg-4 col-md-6 -->

                    <!-- start search-area -->
                    <div class="col-lg-4 col-md-12" style="margin-top: 10px;">
                        <div class="feature" style="box-shadow: 0 2px 50px rgb(0 0 0 / 8%); transition: 0.3s ease; border-radius: 30px;">
                            <div class="feature__img">
                                <img src="<?= site_url('assets/') ?>images/layanan_clubhouse.png" alt="feature" width="120">
                            </div>
                            <div class="feature__title">
                                <h3><b>Club House</b></h3>
                            </div>
                        </div>
                        <!-- end /.feature -->
                    </div>
                    <!-- end /.col-lg-4 col-md-6 -->
                    
                </div>

            </div>
            <!-- end /.row -->
        </div>
        <!-- end /.container -->
    </section>
    <!--================================
    END FEATURE AREA
=================================-->


    <!--================================
    START FEATURED PRODUCT AREA
=================================-->
    <section class="featured-products bgcolor section--padding">
        <!-- start /.container -->

        <div class="container product-title-area" style="border-radius: 30px;">
            
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <h1><b>Tentang</b>
                            <span class="highlighted">Kami</span>
                        </h1>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-6">
                    <img src="<?= site_url('assets/') ?>images/about1.jpg" alt="">
                </div>
                <div class="col-md-6">
                    <div>
                        <h3>Apa Itu <b>Pusat Pengembangan Bisnis</b><br>UIN Sunan Kalijaga Yogyakarta?</h3>
                        <p style="font-size: 13px; margin-top: 20px;">Kami adalah salah satu Unit Pelayanan Teknis yang bertugas untuk mengelola aset-aset usaha UIN Sunan Kalijaga Yogyakarta. Dengan status Pusat Pengembangan Bisnis (PPB) sebagai UPT, maka pengakuan dan kepercayaan teshadap unit ini semakin tinggl tidak safa di internal UIN Sunan Kalijaga melainkan juga di mata stakeholders. Indikatornya, semakin banyak masyarakat yang memanfaatkan jasa PPB UIN Sunan Kalijaga baik untuk keperluan kegiatan individual maupun kegiatan institusi. Kepercayaan juga datang dari perguruan tinggi lain, terutama di lingkup Perguruan Tinggi Agama Islam (PTA) yang juga mulai perints unit pengembangan bisnis. Banyak di antara mereka yang belajar ke PPB UIN Sunan Kalijaga.</p>
                    </div>
                </div>
            </div>
            
            <div class="row" style="margin-top: 60px;">
                <div class="col-md-6">
                    <div>
                        <h3><b>VISI</b></h3>
                        <p style="font-size: 13px; margin-top: 20px;">Menjadi ujung tombak dalam proses pengembangan bisnis UIN Sunan Kalijaga Yogyakarta yang mencerdaskan dan mensejahterakan civitas akademika dan masyarakat.</p>
                        <hr>
                        <h3><b>MISI</b></h3>
                        <p style="font-size: 13px; margin-top: 20px;">Melakukan pembinaan kepada seluruh usaha bisnis dalam lingkup UIN Sunan Kalijaga sesuai dengan prinsip manajemen bisnis yang sehat dan transparan.</p>

                    </div>
                </div>
                <div class="col-md-6">
                    <img src="<?= site_url('assets/') ?>images/about2.jpg" alt="">
                </div>
            </div>
            
            
        </div>

    </section>
    <!--================================
    END FEATURED PRODUCT AREA
=================================-->

