<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\HTTP\CLIRequest;
use CodeIgniter\HTTP\IncomingRequest;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;

class Futsal extends Controller
{
    public function index()
    {
        $data = [
            'title' => 'Futsal | UIN SUKA',
            'description' => 'Futsal UIN SUKA',
        ];

        echo view('view_header', $data);
        echo view('view_futsal', $data);
        echo view('view_footer', $data);
        
    }
}
