<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\HTTP\CLIRequest;
use CodeIgniter\HTTP\IncomingRequest;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;

class Home extends Controller
{
    public function index()
    {
        $data = [
            'title' => 'UIN SUKA',
            'description' => 'Selamat Datang di Web UIN SUKA',
        ];

        echo view('view_header', $data);
        echo view('view_home', $data);
        echo view('view_footer', $data);
        
    }
}
