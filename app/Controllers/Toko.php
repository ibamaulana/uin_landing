<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\HTTP\CLIRequest;
use CodeIgniter\HTTP\IncomingRequest;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;

class Toko extends Controller
{
    public function index()
    {
        $data = [
            'title' => 'Toko | UIN SUKA',
            'description' => 'Toko UIN SUKA',
        ];

        echo view('view_header', $data);
        echo view('view_toko', $data);
        echo view('view_footer', $data);
        
    }
}
